package clasesJAXB;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class LineaAccionTest {

    @Autowired
    LineaAccion lineaAccion;

    @Test
    void createNewLineaAccion() {
        LineaAccion cooperacionAlDesarrollo = new LineaAccion(
                1,
                "COOPERACIÓN AL DESARROLLO",
                new String[]{"Universilización de la educación", "Mejora de la calidad educativa", "Formación para el trabajo", "Promoción social y desarrollo comunitario"},
                "Esta línea de actuación constituye el grueso de nuestro trabajo actualmente. El objetivo es incidir en las problemáticas principales que están generando la vulnerabilidad de las poblaciones en lo relativo a la educación y a su desarrollo."
        );

        Assertions.assertEquals("COOPERACIÓN AL DESARROLLO", cooperacionAlDesarrollo.getNombre());
        Assertions.assertNotNull(cooperacionAlDesarrollo.getSublineasAccion());
    }

    @Test
    void getNombre() {
        lineaAccion = new LineaAccion();
        String nombre = lineaAccion.getNombre();
        Assertions.assertNull(lineaAccion.getNombre());
    }

    @Test
    void setNombre() {
        lineaAccion = new LineaAccion();
        String newName = "Accion Humanitaria";
        lineaAccion.setNombre(newName);
        String nombre = lineaAccion.getNombre();
        Assertions.assertNotNull(lineaAccion.getNombre());
        Assertions.assertEquals("Accion Humanitaria", lineaAccion.getNombre());
    }


}