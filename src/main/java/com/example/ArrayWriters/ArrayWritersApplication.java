package com.example.ArrayWriters;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.bind.JAXBException;


@SpringBootApplication
public class ArrayWritersApplication extends Application {
	

	public static void main(String[] args) throws JAXBException {
			launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("views/home-view.fxml"));
		Scene scene = new Scene(root, 1024, 720);

		stage.setTitle("ONG Application");
		stage.setScene(scene);

		stage.show();

	}
}
