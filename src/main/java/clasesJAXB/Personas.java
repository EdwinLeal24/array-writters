package clasesJAXB;

import java.util.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement (name="personas")

public class Personas{
	List<Persona> personas;
	public List<Persona> getPersonas(){
		return personas;
	}
	
	@XmlElement (name="Persona")
	
	public void setPersonas (List<Persona> personas){
		this.personas = personas;
	}
	
	public void add(Persona persona) {
		if (this.personas == null) {
			this.personas = new ArrayList <Persona>();
		}
		this.personas.add(persona);
	}	
}
