//Package al que pertenece la clase
package clasesJAXB;

//AQUÍ AÑADIREMOS TODOS LOS EXTRAS DE JAXB (1)
import javax.xml.bind.annotation.*;
@XmlRootElement (name = "persona")
@XmlAccessorType (XmlAccessType.FIELD)


//Nombre de la clase
public class Persona {
    //Características de la clase Persona
	//AQUÍ AÑADIREMOS TODOS LOS EXTRAS DE JAXB (2)

	@XmlElement (name="usuario")
	protected String usuario;
	@XmlElement (name="password")
	protected String password;	
	@XmlElement (name="id")
	protected Integer id;
	@XmlElement (name="nombre")
	protected String nombre;	
	@XmlElement (name="profesion")
	protected String profesion;	
	@XmlElement (name="internacional")
	protected Integer internacional;


    //Método constructor de la clase Persona

    public Persona(String Usuario, String Password, Integer Id, String Nombre, String Profesion, Integer Internacional){
        this.usuario = Usuario;
        this.password = Password;
        this.id = Id;
        this.nombre = Nombre;
        this.profesion = Profesion;
        this.internacional = Internacional;
    }
    
    public Persona() {};

    //Métodos set y get de Usuario
    public String getUsuario(){
        return usuario;
    }
    public void setUsuario(String Usuario){
        this.usuario=Usuario;
    }
   
    //Métodos set y get de Password
    public String getPassword(){
        return password;
    }
    public void setPassword(String Password){
        this.password=Password;
    }

    //Métodos set y get de Id
    public Integer getId(){
        return id;
    }
    public void setId(Integer Id){
        this.id=Id;
    }    

    //Métodos set y get de Nombre
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String Nombre){
        this.nombre=Nombre;
    }   

    //Métodos set y get de Profesion
    public String getProfesion(){
        return profesion;
    }
    public void setProfesion(String Profesion){
        this.profesion=Profesion;
    }   

    //Métodos set y get de Internacional
    public Integer getInternacional(){
        return internacional;
    }
    public void setInternacional(Integer Internacional){
        this.internacional=Internacional;
    }   

    //Otras funciones de la clase persona
    
	public String toString() {
		return "Usuario: " + usuario + "Password: " + password + "ID: " + id + "Nombre: " + nombre + "Profesion: " + profesion + "Internacional: " + internacional ;
	}
}
