package clasesJAXB;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "LineasAcciones")
public class LineasAcciones {
    List<LineaAccion> lineasAcciones;

    public List<LineaAccion> getLineasAcciones() {
        return lineasAcciones;
    }

    @XmlElement(name = "Stand")
    public void setLineasAcciones(List<LineaAccion> lineasAcciones) {
        this.lineasAcciones = lineasAcciones;
    }

    public void addLineaAccion(LineaAccion lineaAccion) {
        if(this.lineasAcciones == null) {
            this.lineasAcciones = new ArrayList<LineaAccion>();
        }
        this.lineasAcciones.add(lineaAccion);
    }


}