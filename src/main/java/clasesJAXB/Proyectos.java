package clasesJAXB;

import java.util.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement (name="proyectos")

public class Proyectos{
	List<Proyecto> proyectos;
	public List<Proyecto> getProyectos(){
		return proyectos;
	}
	
	@XmlElement (name="Proyecto")
	
	public void setProyectos (List<Proyecto> proyectos){
		this.proyectos = proyectos;
	}
	
	public void add(Proyecto proyecto) {
		if (this.proyectos == null) {
			this.proyectos = new ArrayList <Proyecto>();
		}
		this.proyectos.add(proyecto);
	}	
}
