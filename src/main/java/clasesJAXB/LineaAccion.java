package clasesJAXB;

import java.util.Arrays;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity(name = "LineaAccion")
@Table(name = "lineaAccion")
public class LineaAccion {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "nombre", length = 250)
    private String nombre;

    @Column(name = "sublineasAccion")
    private String[] sublineasAccion;

    @Column(name = "descripcion")
    private String descripcion;

    // CONSTRUCTORS
    public LineaAccion() {
    }

    public LineaAccion(Integer id, String nombre, String sublineasAccion[], String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.sublineasAccion = sublineasAccion;
        this.descripcion = descripcion;
    }

    // GETTERS & SETTERS
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String[] getSublineasAccion() {
        return sublineasAccion;
    }

    public void setSublineasAccion(String[] sublineasAccion) {
        this.sublineasAccion = sublineasAccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "LineaAccion{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", sublineasAccion=" + Arrays.toString(sublineasAccion) +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}

