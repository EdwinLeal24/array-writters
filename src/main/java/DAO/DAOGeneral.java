package DAO;

import javax.xml.bind.JAXBException;

public abstract class DAOGeneral {

	public static final int XML = 1;
	public static final int MYSQL = 2;
	
	public abstract XmlPersonaDAO getPersonaDAO() throws JAXBException;
	public abstract XmlLineaAccionDAO getLineaAccionDAO() throws JAXBException;
	public abstract XmlProyectoDAO getProyectoDAO() throws JAXBException;
	
	public static DAOGeneral getDAOGeneral (int whichGeneral) {
		switch (whichGeneral) {
		
		case XML:
			return new XmlDAOGeneral();
		default:
			return null;
		}
	}
}
