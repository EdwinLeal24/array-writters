package DAO;

import javax.xml.bind.JAXBException;
import clasesJAXB.Proyectos;



public interface ProyectoDAO {
	public void guardarProyectos (Proyectos proyectos) throws JAXBException;
	public Proyectos listarProyectos() throws JAXBException;
}