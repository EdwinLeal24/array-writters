package DAO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import Database.ConnectDB;
import clasesJAXB.Proyecto;
import clasesJAXB.Proyectos;

import java.io.File;

public class XmlProyectoDAO implements ProyectoDAO{

	private JAXBContext jaxbContext =null;
	private String nombreFichero =null;
	
	public XmlProyectoDAO() throws JAXBException{
		this.jaxbContext = JAXBContext.newInstance(Proyectos.class);
		this.nombreFichero ="Proyectos.xml";	
	}
	
	@Override
	public void guardarProyectos (Proyectos proyectos) throws JAXBException{

		for (Proyecto proyecto : proyectos.getProyectos()) {
			String query = "INSERT INTO proyecto (id, pais, localizacion, fechaInit, fechaFin, financiacionAportada, idLineaAccion, idFinanciador)";
			query = query+" VALUES("+proyecto.getIdProyecto()+", '"+proyecto.getPais()+"', '"+proyecto.getLocalizacion()+"', '"+proyecto.getFechaInit()+"', '"+proyecto.getFechaFin()+"', '"+proyecto.getFinanciacionAportada()+"', '"+proyecto.getSublineaAccion()+"', '"+proyecto.getFinanciador()+"');";
			ConnectDB.saveData(query);
			System.out.println("Persona guardada");
		}

		// ↓ TO SAVE DATA IN XML FILE ↓
		/*
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(proyectos,  new File (nombreFichero));
		System.out.println();
		System.out.println("Se ha escrito el archivo "+ nombreFichero + " con el siguiente contenido:");
		System.out.println();
		marshaller.marshal(proyectos,  System.out);
		 */
	}
	
	@Override
	public Proyectos listarProyectos() throws JAXBException{
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Proyectos proyectos = (Proyectos) unmarshaller.unmarshal( new File(nombreFichero));
		
		System.out.println();
		System.out.println("Estos son los proyectos del archivo: "+nombreFichero);
		
		for(Proyecto proyecto : proyectos.getProyectos()) {
			System.out.println("-");
			System.out.println("Identificador del proyecto: \t" + proyecto.getIdProyecto());
			System.out.println("Pais del proyecto: \t"+proyecto.getPais());
		}
		return null;
			
		}
		
	}
