package DAO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import Database.ConnectDB;
import clasesJAXB.LineaAccion;
import clasesJAXB.LineasAcciones;

import java.io.File;


public class XmlLineaAccionDAO implements ILineaAccionDAO {


    private JAXBContext jaxbContext = null;
    private String xmlNombre = null;

    public XmlLineaAccionDAO() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(LineasAcciones.class);
        this.xmlNombre = "LineasDeAcciones.xml";
    }

    @Override
    public void guardarLineasAcciones(LineasAcciones lineasAcciones) throws JAXBException {
        for (LineaAccion lineaAccion : lineasAcciones.getLineasAcciones()) {
            String query = "INSERT INTO lineaAccion (id, nombre, sublineasAccion, descripcion)";
            query = query+" VALUES("+lineaAccion.getId()+", '"+lineaAccion.getNombre()+"', 'sublineas', 'descripcion');";
            ConnectDB.saveData(query);
            System.out.println("linea guardada");
        }

        // ↓ TO SAVE DATA IN XML FILE ↓
        /*
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(lineasAcciones, new File(xmlNombre));
        System.out.println();
        System.out.println("Se ha escrito el fichero " + xmlNombre + " con el siguiente contenido:");
        System.out.println();
        marshaller.marshal(lineasAcciones, System.out);
         */

    }

    @Override
    public LineasAcciones muestraLineasAcciones() throws JAXBException {

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        LineasAcciones lineasAcciones = (LineasAcciones) unmarshaller.unmarshal( new File(xmlNombre) );

        System.out.println();
        System.out.println("Nuestras Lineas de Acciones son " + xmlNombre);

        for (LineaAccion lineaAccion : lineasAcciones.getLineasAcciones()) {
            System.out.println("***");
            System.out.println("Id linea de acción: \t" + lineaAccion.getId());
            System.out.println("Linea de acción: \t" + lineaAccion.getNombre());
            System.out.println("Descripción: \t" + lineaAccion.getDescripcion());
            System.out.println("Sublineas de accion:");
            for (String sublineaAccion : lineaAccion.getSublineasAccion()) {
                System.out.println("· " + sublineaAccion);
            }
        }

        return null;
    }
}
