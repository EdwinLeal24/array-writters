package DAO;

import javax.xml.bind.JAXBException;

import clasesJAXB.LineasAcciones;

public interface ILineaAccionDAO {
    public void guardarLineasAcciones(LineasAcciones lineasAcciones) throws JAXBException;

    public LineasAcciones muestraLineasAcciones() throws JAXBException;
}
