package DAO;

import javax.xml.bind.JAXBException;
import clasesJAXB.Personas;

public interface PersonaDAO {
	public void guardarPersonas (Personas personas) throws JAXBException;
	public Personas listarPersonas() throws JAXBException;
}
