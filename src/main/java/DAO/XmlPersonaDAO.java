package DAO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import Database.ConnectDB;
import clasesJAXB.LineaAccion;
import clasesJAXB.Persona;
import clasesJAXB.Personas;

import java.io.File;

public class XmlPersonaDAO implements PersonaDAO{

	private JAXBContext jaxbcontext =null;
	private String nombreFichero =null;
	
	public XmlPersonaDAO() throws JAXBException{
		this.jaxbcontext = JAXBContext.newInstance(Personas.class);
		this.nombreFichero ="Personas.xml";	
	}
	
	@Override
	public void guardarPersonas (Personas personas) throws JAXBException{

		for (Persona persona : personas.getPersonas()) {
			String query = "INSERT INTO persona (id, nombre, usuario, password, profesion, internacional)";
			query = query+" VALUES("+persona.getId()+", '"+persona.getNombre()+"', '"+persona.getUsuario()+"', '"+persona.getPassword()+"', '"+persona.getProfesion()+"', '"+persona.getInternacional()+"');";
			ConnectDB.saveData(query);
			System.out.println("Persona guardada");
		}

		// ↓ TO SAVE DATA IN XML FILE ↓
		/*
		Marshaller marshaller = jaxbcontext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(personas,  new File (nombreFichero));
		System.out.println();
		System.out.println("Se la escrito el archivo "+ nombreFichero + " con el siguiente contenido:");
		System.out.println();
		marshaller.marshal(personas,  System.out);
		 */
	}
	
	@Override
	public Personas listarPersonas() throws JAXBException{
		Unmarshaller unmarshaller = jaxbcontext.createUnmarshaller();
		Personas personas = (Personas) unmarshaller.unmarshal( new File(nombreFichero));
		
		System.out.println();
		System.out.println("Estas son las personas contenidas en el archivo"+nombreFichero);
		
		for(Persona persona : personas.getPersonas()) {
			System.out.println("-");
			System.out.println("Identificador de la persona; \t" + persona.getId());
			System.out.println("Nombre de la persona: \t"+persona.getNombre());
		}
		return null;
			
		}
		
	}
