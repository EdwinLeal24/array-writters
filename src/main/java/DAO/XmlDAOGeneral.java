package DAO;

import javax.xml.bind.JAXBException;

public class XmlDAOGeneral extends DAOGeneral {
	
	public XmlPersonaDAO getPersonaDAO() throws JAXBException{
		return new XmlPersonaDAO();
	}
	public XmlLineaAccionDAO getLineaAccionDAO() throws JAXBException{
		return new XmlLineaAccionDAO();
	}	
	public XmlProyectoDAO getProyectoDAO() throws JAXBException{
		return new XmlProyectoDAO();
	}	
}
