//package Database;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class PreparedStatementExample {
//    public static void main(String[] args) {
//        new PreparedStatement().getUser("12");
//    }
//
//    /**
//     * Obtenemos el usuario de la base de datos.
//     * @param id
//     * @return
//     */
//    public List<Object> getUser(String id) {
//        List<Object> result = new ArrayList<Object>();
//
//        String sql = "SELECT * FROM users where id = ?;";
//
//        // Preparamos la conexión y ejecutamos la consulta.
//        try {
//            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/array-writters/user", "root", "");
//            PreparedStatement prepareStatement = connection.prepareStatement(sql);
//            prepareStatement.setInt(1,Integer.parseInt(id)); // el índice de ? es '1', se realiza una comprobación de null/número para 'id'
//            // Ejecuta la sentencia preparada
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        }
//
//        return result;
//    }
//}
