package Database;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

import java.sql.DriverManager;

public class ConnectDB {

    // DATABASE VALUES
    private static String USER = "root";
    private static String PASSWORD = "";
    private static String URL = "jdbc:mysql://localhost:3306/array-writters";

    // GUARDA LOS DATOS EN LA BASE DE DATOS
    public static void saveData(String sql) {
        try {
            Connection db = DriverManager.getConnection(URL, USER, PASSWORD);

            Statement statement = db.createStatement();

            System.out.println("Query ejecutada → ".concat(sql));
            statement.executeUpdate(sql);


        }catch(SQLException e) {
            System.out.println("Error al conectar la base de datos:".concat(Integer.toString(e.getErrorCode())));
            System.out.println("Error al conectar la base de datos:".concat(e.getSQLState()));
            System.out.println("Error al conectar la base de datos:".concat(e.getMessage()));

        }
    }
}
