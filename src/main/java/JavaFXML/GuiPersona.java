package javaFXML;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import static javafx.application.Application.launch;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 *
 * @author hecto
 */

public class GuiPersona extends Application {

    Stage window;
    Scene scene;
    Button button;

    private void verifyData(TextField input, String message){
        try{
            //TODO
        } catch(Exception e){
            System.out.println("Error: " + message);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;
        window.setTitle("AddPersona");

        // Form
        Label userLabel = new Label("Nombre de Usuario:");
        TextField userInput = new TextField();
        Label passLabel = new Label("Contraseña:");
        TextField passInput = new TextField();
        Label idLabel = new Label("ID:");
        TextField idInput = new TextField();
        Label nameLabel = new Label("Nombre personal:");
        TextField nameInput = new TextField();
        Label profLabel = new Label("Profesión:");
        TextField profInput = new TextField();
        Label internacionalLabel = new Label("Internacional:");
        CheckBox internacional =  new CheckBox();

        button = new Button("Añadir Usuario");
        button.setOnAction(e -> verifyData(userInput, userInput.getText()));

        // Layout
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.getChildren().addAll(userLabel, userInput, passLabel, passInput, idLabel, idInput, nameLabel, nameInput, profLabel, profInput, internacionalLabel, internacional, button);

        scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
