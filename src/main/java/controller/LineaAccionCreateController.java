package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LineaAccionCreateController {

    private final static Logger log = Logger.getLogger(LineaAccionCreateController.class);

    @FXML
    public TextField nombre = new TextField();
    @FXML public TextField sublineasAccion = new TextField();
    @FXML public TextArea descripcion = new TextArea();
    @FXML
    public Label msgSuccess;

    @FXML
    private void submitLineaAccion() {
        String name = nombre.getText();
        String actionsSublines = sublineasAccion.getText();
        String description = descripcion.getText();
        System.out.println("Linea de acción: " + name + " " + actionsSublines + " " + description);
        nombre.setText("");
        sublineasAccion.setText("");
        descripcion.setText("");
        msgSuccess.setText(name + " añadida correctamente!");
    }

    @FXML
    private void backHome(ActionEvent event) throws IOException {
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/home-view.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }
}
