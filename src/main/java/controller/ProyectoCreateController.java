package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ProyectoCreateController {

    @FXML
    public TextField pais = new TextField();
    @FXML
    public TextField ciudad = new TextField();
    @FXML
    public DatePicker fechaInit = new DatePicker();
    @FXML
    public DatePicker fechaFin = new DatePicker();
    @FXML
    public TextField financiacion = new TextField();
    @FXML
    public Label msgSuccess;

    @FXML
    private void submitProject() {
        String country = pais.getText();
        String city = ciudad.getText();
//        String initDate = fechaInit.getText();
//        String endDate = fechaFin.getText();
        String amount = financiacion.getText();
        System.out.println("Proyecto: " + country + " " + city + " " + financiacion);
        pais.setText("");
        ciudad.setText("");
        financiacion.setText("");
        msgSuccess.setText("Proyecto en " + country + " creado correctamente!");
    }

    @FXML
    private void backHome(ActionEvent event) throws IOException {
        System.out.println("redirectToProject");
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/home-view.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }
}
