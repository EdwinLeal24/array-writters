package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML
    private void redirectToUser(ActionEvent event) throws IOException {
        System.out.println("redirectToUser");
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/personas-create.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }

    @FXML
    private void redirectToProject(ActionEvent event) throws IOException {
        System.out.println("redirectToProject");
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/proyecto-create.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }

    @FXML
    private void redirectToAction(ActionEvent event) throws IOException {
        System.out.println("redirectToAction");
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/lineas-accion.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
