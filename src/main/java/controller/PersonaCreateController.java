package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class PersonaCreateController {
    @FXML
    public TextField nombre = new TextField();
    @FXML
    public TextField usuario = new TextField();
    @FXML
    public TextField contraseña = new TextField();
    @FXML
    public TextField profesion = new TextField();
    @FXML
    public CheckBox internacional = new CheckBox();
    @FXML
    public Label msgSuccess;

    @FXML
    private void submitNewPerson() {
        String name = nombre.getText();
        String user = usuario.getText();
        String password = contraseña.getText();
        String profession = profesion.getText();
        String international = internacional.getText();
        System.out.println("Persona: " + name + " " + user + " " + password + " " + profession  + " " + international);
        nombre.setText("");
        usuario.setText("");
        contraseña.setText("");
        profesion.setText("");
        internacional.setText("");
        msgSuccess.setText("Usuario " + name + " añadido correctamente");
    }

    @FXML
    private void backHome(ActionEvent event) throws IOException {
        Parent create_action_parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/home-view.fxml"));
        Scene create_action_scene = new Scene(create_action_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(create_action_scene);
        app_stage.show();
    }

}
