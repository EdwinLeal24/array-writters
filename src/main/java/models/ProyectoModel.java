package models;


import javax.persistence.Column;
import javax.persistence.Id;

public class ProyectoModel {

        //Características de la clase Proyecto
        @Id
        @Column(name="idproyecto")
        protected int idProyecto;
        @Column(name="pais")
        protected String pais;
        @Column(name="id")
        protected String id;
        @Column(name="localizacion")
        protected String localizacion;
        @Column(name="sublineaAccion")
        protected Integer sublineaAccion;
        @Column(name="fechaInit")
        protected String fechaInit;
        @Column(name="fechaFin")
        protected String fechaFin;
        @Column(name="financiador")
        protected Integer financiador;
        @Column(name="financiacionAportada")
        protected int financiacionAportada;


        //Constructor de la clase Proyecto

        public ProyectoModel(int idProyecto, String pais, String id, String localizacion,Integer sublineaAccion, String fechaInit, String fechaFin, Integer financiador, int financiacionAportada){
            this.idProyecto = idProyecto;
            this.pais = pais;
            this.id =id; // De que es este id??
            this.localizacion = localizacion;
            this.sublineaAccion = sublineaAccion;
            this.fechaInit = fechaInit;
            this.fechaFin = fechaFin;
            this.financiador = financiador;
            this.financiacionAportada = financiacionAportada;
        }

        public ProyectoModel() {};

        //Setter y getters de IdProyecto

        public int getIdProyecto(){
            return idProyecto;
        }
        public void setIdProyecto(int IdProyecto){
            this.idProyecto=IdProyecto;
        }

        //Setter y getters de Pais

        public String getPais(){
            return pais;
        }
        public void setPais(String Pais){
            this.pais=Pais;
        }

        //Setter y getters de Localizacion

        public String getLocalizacion(){
            return localizacion;
        }
        public void setLocalizacion(String Localizacion){
            this.localizacion=Localizacion;
        }


        //Setter y getters de SublineaAccion

        public Integer getSublineaAccion(){
            return sublineaAccion;
        }
        public void setSublineaAccion(Integer SublineaAccion){
            this.sublineaAccion=SublineaAccion;
        }

        //Setter y getters de DateInit

        public String getFechaInit(){
            return fechaInit;
        }
        public void setDateInit(String FechaInit){
            this.fechaInit=FechaInit;
        }

        //Setter y getters de DateFin

        public String getFechaFin(){
            return fechaFin;
        }
        public void setDateFin(String FechaFin){
            this.fechaFin=FechaFin;
        }


        //Setter y getters de Financiador

        public Integer getFinanciador(){
            return financiador;
        }
        public void setFinanciador(Integer Financiador){
            this.financiador=Financiador;
        }

        //Setter y getters de FinanciacionAportada

        public int getFinanciacionAportada(){
            return financiacionAportada;
        }
        public void setFinanciacionAportada(int FinanciacionAportada){
            this.financiacionAportada=FinanciacionAportada;
        }



        //Funciones adicionales

        public void agregarProyecto() {
            System.out.println("Agregar un proyecto a la Base de Datos.");
        }
        public void eliminarProyecto() {
            System.out.println("Borrar un proyecto de la Base de Datos.");
        }
        public void modificarProyecto() {
            System.out.println("Modificar un proyecto en la Base de Datos.");
        }
        public String toString() {
            return "ID Proyecto: " + idProyecto + "Pais: " + pais + "Localizacion: " + localizacion +
                    "Sublinea de Accion: " + sublineaAccion + "Fecha Comienzo: " + fechaInit + "Fecha Fin: " + fechaFin +
                    "Financiador: " + financiador + "Financiacion Aportada: " + financiacionAportada;
        }
    }


