package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "persona")
@Table(name = "persona")
public class PersonaModel{
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "usuario", length = 250)
    private String usuario;

    @Column(name = "password", length = 250)
    private String password;

    @Column(name = "nombre")
    private String[] nombre;

    @Column(name = "profesion")
    private String profesion;

    @Column(name = "internacional")
    private String internacional;

    // CONSTRUCTORS
    public PersonaModel() {
    }

    public PersonaModel(Integer id, String usuario, String password, String[] nombre, String profesion, String internacional) {
        this.id = id;
        this.usuario = usuario;
        this.password = password;
        this.nombre = nombre;
        this.profesion = profesion;
        this.internacional = internacional;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getNombre() {
        return nombre;
    }

    public void setNombre(String[] nombre) {
        this.nombre = nombre;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getInternacional() {
        return internacional;
    }

    public void setInternacional(String internacional) {
        this.internacional = internacional;
    }
}
